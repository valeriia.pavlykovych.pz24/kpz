﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Event1 basket = new Event1(3);
            basket.Notify += DisplayMessage;   // Добавляем обработчик для события Notify
            basket.Put(5);    // добавляем на счет 20
            printApplesAvailable(basket.Num);
            basket.Take(7);   // пытаемся снять со счета 70
            printApplesAvailable(basket.Num);
            basket.Take(5);  // пытаемся снять со счета 180
            printApplesAvailable(basket.Num);
            Console.WriteLine("-------------------------------------");

            Event2 amazingsetter = new Event2(false);
            amazingsetter.Notify += ShowMessage;
            amazingsetter.makeAmazing();
            amazingsetter.notAmazing();
            amazingsetter.notAmazing();

            Console.Read();
        }
        private static void DisplayMessage(string message)
        {
            Console.WriteLine(message);
        }

        private static void printApplesAvailable(int n)
        {
            Console.WriteLine($"Apples in the basket: {n}\n");
        }

        private static void ShowMessage(string message)
        {
            Console.WriteLine(message);
        }
    }
}
