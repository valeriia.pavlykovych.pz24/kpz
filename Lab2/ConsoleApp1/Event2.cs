﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Event2
    {
        public delegate void AmazingHandler(string message);
        public event AmazingHandler Notify;

        public Event2(bool isAmazing)
        {
            IsAmazing = isAmazing;
        }
        public bool IsAmazing { get; private set; }
        public void makeAmazing()
        {
            IsAmazing = true;
            Notify?.Invoke($"Well, now it's amazing!");   // 1 Notify 
        }
        public void notAmazing()
        {
            if (IsAmazing)
            {
                IsAmazing = false;
                Notify?.Invoke("Oh no! Not amazing anymore :(");   // 2 Notify 
            }
            else
            {
                Notify?.Invoke($"It's AREADY NOT amazing"); // 3 Notify 
            }
        }
    }
}
