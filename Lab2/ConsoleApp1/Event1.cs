﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Event1
    {
        public delegate void AppleHandler(string message);
        public event AppleHandler Notify;               
        public Event1(int num)
        {
            Num = num;
        }
        public int Num { get; private set; }
        public void Put(int num)
        {
            Num += num;
            Notify?.Invoke($"Apples added to the basket: {num}");   //  1 Notify
        }
        public void Take(int num)
        {
            if (Num >= num)
            {
                Num -= num;
                Notify?.Invoke($"Apples taken from the basket: {num}");   // 2 Notify 
            }
            else
            {
                Notify?.Invoke($"Not enough apples in the basket! {num} is just too many!"); // 3 Notify 
            }
        }
    }
}
