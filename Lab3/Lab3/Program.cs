﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    class Program

    {
        static void Main(string[] args)
        {
            ModelTests tests = new ModelTests();
            tests.printLists();

            tests.printDictionary();

            tests.otherStructuresOperations();

            ListToArray makeReverseArray = new ListToArray();
            makeReverseArray.makeArray();

            Console.Read();
        }
    }
}
