﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    public class ReverserClass : IComparer
    {
        // Call CaseInsensitiveComparer.Compare with the parameters reversed.
        int IComparer.Compare(Object x, Object y)
        {
            return ((new CaseInsensitiveComparer()).Compare(y, x));
        }
    }
    public class Teacher
    {
        public Teacher(string name, int iD, string course)
        {

            Name = name;
            ID = iD;
            Course = course;
        }

        public string Name { get; set; }
        public int ID { get; set; }
        public string Course { get; set; }
    }
    public class Course
    {
        public Course(string name, int id)
        {
            Name = name;
            ID = id;
        }
        public string Name { get; set; }
        public int ID { get; set; }
    }

    public class ModelTests
    {
        public List<Teacher> Teachers
        {
            get
            {
                return new List<Teacher>()
                {
                new Teacher("Mr. Black", 001, "OOP"),
                new Teacher("Mr. White", 002, "BD"),
                new Teacher("Mr. Red", 003, "AVPZ"),
                new Teacher("Mr. Red", 004, "KPZ"),
                new Teacher("Mr. White", 005, "MAPZ"),
                };
            }
        }

        public List<Course> Сourses =  new List<Course>()
            {
                new Course("OOP", 001), 
                new Course("BD", 002),
                new Course("AVPZ", 003),
                new Course("KPZ", 004),
                new Course("MAPZ", 005),
             };

        public void printLists()
        {
            Console.Write("---------LISTS-----------\n");
            var selectedTeachers = from t in Teachers
                                  orderby t.Name
                                    select t; 
           
            foreach (Teacher s in selectedTeachers)
                Console.WriteLine(String.Format("{0} {1} {2}", s.Name, s.Course, s.ID));

            var selectedCourses = from c in this.Сourses
                                   orderby c.Name
                                   select c; 

            foreach (Course c in selectedCourses)
                Console.WriteLine(String.Format("{0} {1}", c.Name, c.ID));

            var selectedCourses2 = from c in this.Сourses
                                   where c.Name.StartsWith("O")
                                   select c;

            Console.Write("\n--SELECT ONLY COURSES [O%]--\n");
            foreach (Course c in selectedCourses2)
                Console.WriteLine(String.Format("{0} {1}", c.Name, c.ID));

        }

        public void printDictionary()
        {
            Console.Write("\n-------DICTIONARY---------");
            Dictionary<string, List<Course>> teacherAndCourses = new Dictionary<string, List<Course>>();
            teacherAndCourses.Add("Mr. Black", new List<Course>() { new Course("OOP", 001) });
            teacherAndCourses.Add("Mr. White", new List<Course>() { new Course("BD", 002), new Course("AVPZ", 003) });
            teacherAndCourses.Add("Mr. Red", new List<Course>() { new Course("KPZ", 004), new Course("MAPZ", 005) });

            foreach (KeyValuePair<string, List<Course>> keyValue in teacherAndCourses)
            {
                Console.Write("\n\nTeacher: " + keyValue.Key + "\nCourses:");
                foreach (Course c in keyValue.Value)
                {
                    Console.Write(c.Name + " ");
                }
            }
        }

        public class Student
        {
            public string Name { get; set; }
        }

        public void otherStructuresOperations()
        {
            
            Queue<Student> studs = new Queue<Student>();
            studs.Enqueue(new Student() { Name = "Lena" });
            studs.Enqueue(new Student() { Name = "Don" });
            studs.Enqueue(new Student() { Name = "John" });

            Console.Write("\n\n----------QUEUE------------\n\nPeek 1 student: ");
            Student pp = studs.Peek(); //not dequeue, just look
            Console.Write(pp.Name);

            Console.WriteLine("\n\nStudents in queue: {0}", studs.Count);

            foreach (Student p in studs)
            {
                Console.WriteLine(p.Name);
            }

            Student stud = studs.Dequeue(); // -Lena
            Console.WriteLine("\nStudent dequeued - " + stud.Name);

            Console.WriteLine("\nStudents in queue: {0}", studs.Count);

            foreach (Student p in studs)
            {
                Console.WriteLine(p.Name);
            }
        }
    }

    public class ListToArray
    {
        public List<int> course { get; private set; }
        public void makeArray ()
        {
            course = new List<int>() { 1, 2, 3, 4, 5 };
            int[] subjectIDs = course.ToArray();

            //reversing using IComparer
            Array.Sort(subjectIDs, new ReverserClass());

            Console.Write("\n\n--REVERSED ARRAY (FROM LIST)--\n");
            foreach (var item in subjectIDs)
            {
                Console.Write(item);
            }
        }
    }
    public static class List_Extension
    {
        public static int CharCount(List <string> list)
        {
            int counter = 0;
            foreach(string s in list)
            {
                counter += s.Length;
            }
            return counter;
        }

        public static void AddToFront<T>(this List<T> list, T item)
        {
            list.Insert(0, item);
        }
    }
}
