﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace Pavlykovych.Test
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void TestFindDistance1()
        {
            Position a = new Position
            {
                X = 2,
                Y = 2
            };

            Position b = new Position
            {
                X = 1,
                Y = 1
            };

            Assert.AreEqual(2, Pavlykovych.Valeriia.RobotChallange.DistanceHelper.FindDistance(a,b));

        }

        [TestMethod]
        public void TestFindDistance2()
        {
            Position a = new Position
            {
                X = 2,
                Y = 2
            };

            Position b = new Position
            {
                X = 1,
                Y = 1
            };

            Assert.AreNotEqual(3, Pavlykovych.Valeriia.RobotChallange.DistanceHelper.FindDistance(a, b));
        }

        [TestMethod]
        public void TestFindIfOutsideMap1()
        {
            Position a = new Position
            {
                X = 2,
                Y = 2
            };

            Assert.AreEqual(true, Pavlykovych.Valeriia.RobotChallange.DistanceHelper.NotOutsideMap(a));
        }

        [TestMethod]
        public void TestFindIfOutsideMap2()
        {
            Position a = new Position
            {
                X = 101,
                Y = 101
            };

            Assert.AreEqual(false, Pavlykovych.Valeriia.RobotChallange.DistanceHelper.NotOutsideMap(a));
        }

        [TestMethod]
        public void TestFindIfOutsideMap3()
        {
            Position a = new Position
            {
                X = 101,
                Y = 99
            };

            Assert.AreEqual(false, Pavlykovych.Valeriia.RobotChallange.DistanceHelper.NotOutsideMap(a));
        }

        [TestMethod]
        public void TestFindIfEnoughEnergy1()
        {
            Robot.Common.Robot movingRobot = new Robot.Common.Robot
            {
                Energy = 5
            };

            int distance = 3;

            Assert.AreEqual(true, Pavlykovych.Valeriia.RobotChallange.DistanceHelper.HasEnoughEnergy(movingRobot, distance));
        }

        [TestMethod]
        public void TestFindIfEnoughEnergy2()
        {
            Robot.Common.Robot movingRobot = new Robot.Common.Robot
            {
                Energy = 3
            };

            int distance = 3;

            Assert.AreEqual(true, Pavlykovych.Valeriia.RobotChallange.DistanceHelper.HasEnoughEnergy(movingRobot, distance));
        }

        [TestMethod]
        public void TestFindIfEnoughEnergy3()
        {
            Robot.Common.Robot movingRobot = new Robot.Common.Robot
            {
                Energy = 1
            };

            int distance = 3;

            Assert.AreEqual(false, Pavlykovych.Valeriia.RobotChallange.DistanceHelper.HasEnoughEnergy(movingRobot, distance));
        }

        [TestMethod]
        public void TestIfCellFree1()
        {
            List<Robot.Common.Robot> list = new List<Robot.Common.Robot>();
            
            Position cell = new Position
            {
                X = 2,
                Y = 2
            };

            Position myRobotPos = new Position
            {
                X = 1,
                Y = 1
            };

            Robot.Common.Robot myRobot = new Robot.Common.Robot
            {
                Position = myRobotPos
            };

            Position anotherRobotPos = new Position
            {
                X = 2,
                Y = 2
            };

            Robot.Common.Robot anotherRobot = new Robot.Common.Robot
            {
                Position = anotherRobotPos
            };

            list.Add(anotherRobot);

            Assert.AreEqual(false, Pavlykovych.Valeriia.RobotChallange.DistanceHelper.IsCellFree(cell, myRobot, list));
        }

        [TestMethod]
        public void TestIfCellFree2()
        {
            List<Robot.Common.Robot> list = new List<Robot.Common.Robot>();

            Position cell = new Position
            {
                X = 3,
                Y = 3
            };

            Position myRobotPos = new Position
            {
                X = 1,
                Y = 1
            };

            Robot.Common.Robot myRobot = new Robot.Common.Robot
            {
                Position = myRobotPos
            };

            Position anotherRobotPos = new Position
            {
                X = 2,
                Y = 2
            };

            Robot.Common.Robot anotherRobot = new Robot.Common.Robot
            {
                Position = anotherRobotPos
            };

            list.Add(anotherRobot);

            Assert.AreEqual(true, Pavlykovych.Valeriia.RobotChallange.DistanceHelper.IsCellFree(cell, myRobot, list));
        }
    }
}
