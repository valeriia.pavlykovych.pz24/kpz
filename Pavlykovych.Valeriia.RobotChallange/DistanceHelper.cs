﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pavlykovych.Valeriia.RobotChallange
{
    public static class DistanceHelper
    {
        public static bool stop = false;
        public static int FindDistance(Position a, Position b)
        {
            return (int)(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }

        public static Position FindNearestFreeStation(Robot.Common.Robot movingRobot, Map map,
            IList<Robot.Common.Robot> robots)
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            foreach (var station in map.Stations)
            {
                if (IsStationFree(station, movingRobot, robots))
                {
                    int d = DistanceHelper.FindDistance(station.Position, movingRobot.Position);

                    if (d < minDistance)
                    {
                        minDistance = d;
                        nearest = station;
                    }
                }
            }
            return nearest == null ? null : nearest.Position;
        }

        public static bool IsStationFree(EnergyStation station, Robot.Common.Robot movingRobot,
            IList<Robot.Common.Robot> robots)
        {
            return IsCellFree(station.Position, movingRobot, robots);
        }

        public static bool IsCellFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot != movingRobot)
                {
                    if (robot.Position == cell)
                        return false;
                }
            }
            return true;
        }

        public static bool HasEnoughEnergy(Robot.Common.Robot movingRobot, int distance)
        {
            if (movingRobot.Energy < distance)
            {
                return false;
            }
            return true;
        }

        public static bool NotOutsideMap(Position toCheck)
        {
            if ((toCheck.X > 99) || (toCheck.Y > 99) || (toCheck.X < 0) || (toCheck.Y < 0))
                return false;
            return true;
        }

        public static Position CalculateNextPosition(Robot.Common.Robot robot, Position position)
        {
            Position from = robot.Position;
            int steps = CalculateSteps(robot, position);
            Position to = new Position(from.X + (position.X - from.X) / steps, from.Y + (position.Y - from.Y) / steps);
            return to;
        }
        public static int CalculateSteps(Robot.Common.Robot robot, Position station)
        {
            return CalculateSteps_Internal(robot, 1, station);
        }
        private static int CalculateSteps_Internal(Robot.Common.Robot robot, int divider, Position station)
        {
            Position intermediate = null;
            if (divider == 1)
            {
                intermediate = station;
            }
            else
            {
                int interm_X = (station.X - robot.Position.X) / divider;
                int interm_Y = (station.Y - robot.Position.Y) / divider;

                intermediate = new Position((robot.Position.X + interm_X), (robot.Position.Y + interm_Y));
            }
            int energy = (FindDistance(robot.Position, intermediate) * divider) + 50;


            if (divider > 40)
            {
                stop = true;
            }
            if (robot.Energy < 10 || stop)
            {
                stop = false;
                return 1;
            }
            if (energy > robot.Energy || energy > 300)
            {
                ++divider;
                return CalculateSteps_Internal(robot, divider, station);
            }
            else
            {
                return divider;
            }
        }
    }
}
