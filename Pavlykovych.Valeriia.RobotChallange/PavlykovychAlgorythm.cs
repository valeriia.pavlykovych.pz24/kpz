﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pavlykovych.Valeriia.RobotChallange
{
    public class PavlykovychAlgorythm : IRobotAlgorithm
    {
        public string Author
        {
            get { return "Pavlykovych Valeriia"; }
        }

        public string Description
        {
            get
            {
                return
                    String.Concat(
                        "This is my first c# algorithm. ");
            }
        }

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot movingRobot = robots[robotToMoveIndex];
            if((movingRobot.Energy > 500) && (robots.Count < map.Stations.Count))
            {
                return new CreateNewRobotCommand();
            }

            Position stationPosition = DistanceHelper.FindNearestFreeStation(robots[robotToMoveIndex], map, robots);

            Position toMove = DistanceHelper.CalculateNextPosition(movingRobot, stationPosition);

            if (toMove == null)
                return null;

            if (toMove == movingRobot.Position)
                return new CollectEnergyCommand();
            else
            {
                return new MoveCommand() { NewPosition = toMove };
            }
        }
    }
}
